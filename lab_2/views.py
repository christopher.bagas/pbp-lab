from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers 

def index(request):
    #Mereturn isi semua object Note dengan template html
    note = Note.objects.all()
    response = {'notes': note}
    return render(request, 'lab_2.html', response)

def xml(request):
    #Mengubah object Note menjadi xml
    data = Note.objects.all()
    data = serializers.serialize('xml', data)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    #Mengubah object Note menjadi json
    data = Note.objects.all()
    data = serializers.serialize('json', data)
    return HttpResponse(data, content_type="application/json")