from django.contrib import admin
from .models import Note

#Menambahkan model Note ke dalam admin
admin.site.register(Note)
