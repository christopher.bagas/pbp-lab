from django.urls import path
from .views import index,xml,json

urlpatterns = [
    #Menambahkan path sesuai dengan urls
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]
