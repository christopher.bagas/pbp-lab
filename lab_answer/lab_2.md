1. Perbedaan antara JSON dan XML
JSON:
- Merupakan notasi objek JavaScript
- Mendukung tipe string, number, boolean, dan array
- Menggunakan struktur data map
- Berorientasi pada data
- Tidak mendukung comments
XML:
- Merupakan bahasa markup
- Hanya mendukung tipe string
- Menggunakan struktur data tree
- Berorientasi pada dokumen
- Mendukung comments
2. Perbedaan antara XML dan HTML
XML:
- Digunakan untuk transfer data
- Tidak boleh ada error dalam coding
- Case sensitive
- Bersifat dynamic
- Harus menggunakan tag penutup
HTML:
- Digunakan untuk menampilkan data
- Diperbolehkan sedikit error dalam coding
- Tidak case sensitive
- Bersifat static
- Tidak harus menggunakan tag penutup