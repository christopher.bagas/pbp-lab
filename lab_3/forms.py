from django import forms
from lab_1.models import Friend

class DateInput(forms.DateInput):
    #Membuat class untuk date widget
    input_type='date'
class FriendForm(forms.ModelForm):
    class Meta:
        #Mengambil model Friend
        model=Friend
        #Melist semua fields yang perlu diisi
        fields = [
            'name',
            'npm',
            'DOB'
        ]
        #Mengapply widget date ke DOB
        widgets={
            'DOB':DateInput()
        }
