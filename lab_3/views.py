from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

@login_required(login_url="/admin/login")#Untuk authorization
def index(request):
    friends = Friend.objects.all() #Ambil semua object Friend
    response = {'friends': friends} #Membuat dict untuk response
    return render(request, 'lab3_index.html', response) #Return ke dalam bentuk html

@login_required(login_url="/admin/login")
def add_friend(request):
    friends_form=FriendForm() #Membuat FriendForm
    response={'friend_form':friends_form} #Membuat dict untuk response
    
    if request.method=="POST": #Jika respnse method POST get isi dari request post 
        friends_form=FriendForm(request.POST)
        if friends_form.is_valid(): #Jika semua field terisi, save ke database
            friends_form.save()
            return redirect('/lab-3') #Redirect ke lab-3

    return render(request,'lab3_form.html', response) #Return FriendForm ke dalam bentuk html