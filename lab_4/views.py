from django.shortcuts import render, redirect
from lab_2.models import Note
from django.http.response import HttpResponse
from .forms import NoteForm

def index(request):
    #Mereturn isi semua object Note dengan template html
    note = Note.objects.all()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)
def add_note(request):
    notes_form=NoteForm() #Membuat NoteForm
    response={'note_form':notes_form} #Membuat dict untuk response
    
    if request.method=="POST": #Jika respnse method POST get isi dari request post 
        notes_form=NoteForm(request.POST)
        if notes_form.is_valid(): #Jika semua field terisi, save ke database
            notes_form.save()
            return redirect('/lab-4') #Redirect ke lab-3

    return render(request,'lab4_form.html', response) #Return NoteForm ke dalam bentuk html

def note_list(request):
    #Mereturn isi semua object Note dengan template html
    note = Note.objects.all()
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)