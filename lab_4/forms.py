from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        #Mengambil model Friend
        model=Note
        #Melist semua fields yang perlu diisi
        fields = [
            'To',
            'From',
            'Title',
            'Message'
        ]