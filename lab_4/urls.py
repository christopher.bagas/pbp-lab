from django.urls import path
from .views import index, add_note,note_list

app_name="lab_4"
urlpatterns = [
    #Menambahkan path sesuai dengan urls
    path('', index, name='index'),
    path('add', add_note, name='add_note'),
    path('note', note_list, name="note_list")
]
